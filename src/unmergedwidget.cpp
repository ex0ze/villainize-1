#include "unmergedwidget.h"
#include "ui_unmergedwidget.h"

UnmergedWidget::UnmergedWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UnmergedWidget)
{
    ui->setupUi(this);
    connect(ui->usepwd_checkbox, &QCheckBox::stateChanged, [&](int st) {
        if (st == Qt::CheckState::Checked) {
            ui->pass_edit->setEnabled(true);
        }
        else ui->pass_edit->setEnabled(false);
    });
}

void UnmergedWidget::setMerger(FileMerger *merger)
{
    m_merger = merger;
}

UnmergedWidget::~UnmergedWidget()
{
    delete ui;
}

void UnmergedWidget::on_openfile_btn_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select a secret file", QDir::currentPath());
    if (filename.isEmpty()) return;
    m_merger->mergeFile(filename.toUtf8());
    ui->filename_label->setText(m_merger->mergedFilename());
    ui->savefile_btn->setEnabled(true);
}

void UnmergedWidget::on_savefile_btn_clicked()
{
    QString filter = QString("(*") + m_merger->mainFileformat() + ")";
    QString abs_name = QFileDialog::getSaveFileName(this, "Enter save filename", QDir::currentPath(), filter);
    if (abs_name.isEmpty()) return;
    if (ui->usepwd_checkbox->isChecked()) {
        m_merger->setPassword(ui->pass_edit->text().toUtf8());
    }
    if (m_merger->writeFile(abs_name.toUtf8())) {
        QMessageBox::information(this, "Ok", "Successfully saved file");
    }
    else QMessageBox::critical(this, "Error", "Can't save file");
}
