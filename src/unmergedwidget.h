#ifndef UNMERGEDWIDGET_H
#define UNMERGEDWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>
#include "filemerger.h"
namespace Ui {
class UnmergedWidget;
}

class UnmergedWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UnmergedWidget(QWidget *parent = nullptr);
    void setMerger(FileMerger* merger);
    ~UnmergedWidget();

private slots:

    void on_openfile_btn_clicked();

    void on_savefile_btn_clicked();

private:
    Ui::UnmergedWidget *ui;
    FileMerger* m_merger;
};

#endif // UNMERGEDWIDGET_H
