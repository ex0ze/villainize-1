#ifndef MERGEDWIDGET_H
#define MERGEDWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>
#include "filemerger.h"

namespace Ui {
class MergedWidget;
}

class MergedWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MergedWidget(QWidget *parent = nullptr);
    void setMerger(FileMerger* merger);
    ~MergedWidget();

private slots:
    void on_save_btn_clicked();
    void on_confirm_btn_clicked();
    void on_cut_btn_clicked();

signals:

private:
    Ui::MergedWidget *ui;
    FileMerger* m_merger;
};

#endif // MERGEDWIDGET_H
