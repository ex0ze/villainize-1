#ifndef FILE_STRUCTS_H
#define FILE_STRUCTS_H

#include <ctype.h>
#include <string.h>

#pragma pack(push,1)

typedef struct {
    char file_name[64];
    size_t file_size;
    size_t file_size_nopadding;
    unsigned int ident;
    bool encryption_flag;
    char encryption_hash_sha512[128];
    unsigned char enctyption_iv[16];
} merged_file_info_t;

#pragma pack(pop)


#define MERGED_IDENT_DEFAULT 0x8CEF0000
#define MERGED_IDENT_1 0xFC00003F


static bool is_valid_mfit_ident(merged_file_info_t* d, int* ver) {
    if (ver) {
        switch (d->ident) {
        case MERGED_IDENT_DEFAULT:
            *ver = 0;
            break;
        case MERGED_IDENT_1:
            *ver = 1;
            break;
        default:
            *ver = -1;
            break;
        }
    }
    return (d->ident == MERGED_IDENT_1 || d->ident == MERGED_IDENT_DEFAULT);
}

static void set_valid_mfit_ident(merged_file_info_t* d, int ver = 0) {
    switch (ver) {
    case 0:
        d->ident = MERGED_IDENT_DEFAULT;
    case 1:
        d->ident = MERGED_IDENT_1;
        return;
    default:
        return;
    }
}

static void create_mfit(merged_file_info_t* d, int ver, const char* file_name, size_t file_size, size_t file_size_np, bool use_enc = false, const char* enc_128_hash = nullptr, unsigned char* enc_16_iv = nullptr) {
    memset(d, 0, sizeof(merged_file_info_t));
    strcpy(d->file_name, file_name);
    d->file_size = file_size;
    d->file_size_nopadding = file_size_np;
    set_valid_mfit_ident(d, ver);
    if (use_enc) {
        d->encryption_flag = true;
        memcpy(d->encryption_hash_sha512, enc_128_hash, 128);
        memcpy(d->enctyption_iv, enc_16_iv, 16);
    }
}

#endif // FILE_STRUCTS_H
