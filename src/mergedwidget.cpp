#include "mergedwidget.h"
#include "ui_mergedwidget.h"

MergedWidget::MergedWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MergedWidget)
{
    ui->setupUi(this);
}

void MergedWidget::setMerger(FileMerger *merger)
{
    m_merger = merger;
    ui->mergedname_lable->setText(m_merger->mergedFilename());
    ui->passflag_label->setText(m_merger->isPasswordProtected() ? "True" : "False");
    if (m_merger->isPasswordProtected()) {
        ui->save_btn->setEnabled(false);
        ui->cut_btn->setEnabled(false);
        ui->groupBox->setEnabled(true);
    }
}

MergedWidget::~MergedWidget()
{
    delete ui;
}

void MergedWidget::on_save_btn_clicked()
{
    auto saveDir = QFileDialog::getExistingDirectory(this, "Select dir to save a file", QDir::currentPath());
    if (saveDir.isEmpty()) return;
    QString abs_name = saveDir + "/" + QString(m_merger->mergedFilename());
    if (m_merger->writeMergedpart(abs_name.toUtf8())) {
        QMessageBox::information(this, "Ok", "Successfully saved file");
    }
    else {
        QMessageBox::critical(this, "Error", "Can't save file");
    }
}

void MergedWidget::on_confirm_btn_clicked()
{
    QString pass = ui->pass_edit->text();
    if (!m_merger->enterPassword(pass.toUtf8())) {
        QMessageBox::critical(this, "Error", "Wrong password");
    }
    else {
        QMessageBox::information(this, "Ok", "Password is correct");
        ui->save_btn->setEnabled(true);
        ui->cut_btn->setEnabled(true);
        ui->groupBox->setEnabled(false);
    }
}

void MergedWidget::on_cut_btn_clicked()
{
    auto cutDir = QFileDialog::getExistingDirectory(this, "Select dir to save a file", QDir::currentPath());
    if (cutDir.isEmpty()) return;
    QString abs_name = cutDir + "/" + QString(m_merger->mergedFilename());
    if (m_merger->cutMergedpart(abs_name.toUtf8())) {
        QMessageBox::information(this, "Ok", "Successfully cut file");
        ui->save_btn->setEnabled(false);
        ui->cut_btn->setEnabled(false);
    }
    else {
        QMessageBox::critical(this, "Error", "Can't cut file");
    }
}
