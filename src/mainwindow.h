#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include "filemerger.h"
#include "mergedwidget.h"
#include "unmergedwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_openfile_btn_clicked();
    void on_mergedFile();
    void on_unmergedFile();
private:
    Ui::MainWindow *ui;
    FileMerger* m_merger;
    MergedWidget* m_mergedwidget;
    UnmergedWidget* m_unmergedwidget;
    QString m_file;
};
#endif // MAINWINDOW_H
