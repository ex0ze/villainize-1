#ifndef FILEMERGER_H
#define FILEMERGER_H

#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <QCryptographicHash>
#include <QDir>
#include "file_structs.h"
#define CBC 1

extern "C" {
#include "aes.h"
}

#ifdef _WIN32
#include <io.h>
#define access _access_s
#else
#include <unistd.h>
#endif

class FileMerger
{

public:

    enum FType {
        UNDEFINED = 0,
        UNMERGED,
        MERGED
    };
    FileMerger();
    bool readFile(const char* filename);

    bool writeFile(const char* filename);
    bool writeMergedpart(const char* filename);
    bool cutMergedpart(const char* filename);
    bool writeMainpart(const char* filename);

    bool isOpened() const;
    bool isPasswordProtected() const;
    bool isMerged() const;

    bool mergeFile(const char* filename);
    bool setPassword(const char* pass);
    bool enterPassword(const char* pass);

    void clearMainpart();
    void clearMergedpart();

    const char* mergedFilename() const;
    const char* mainFilename() const;
    const char* mainFileformat() const;

    static std::size_t fileSize(const char* filename);
    static bool fileExists(const char* filename);

private:
    std::vector<unsigned char> m_mainpart;
    std::vector<unsigned char> m_mergedpart;
    std::string m_mainfilename;
    std::string m_mergedfilename;
    std::string m_mainfileformat;
    merged_file_info_t m_info;
    FType m_filetype;
    bool m_readFlag;

    bool grabHeader(std::ifstream& in, std::size_t sz);
    bool splitFiles(std::ifstream& in, std::size_t sz);

    void fillIv(unsigned char* data);

    void applyPadding();
};

struct MatchPathSeparator
{
    bool operator()( char ch ) const
    {
#ifdef _WIN32
        return ch == '\\' || ch == '/';
#else
        return ch == '/';
#endif
    }
};

#endif // FILEMERGER_H
