#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_merger = new FileMerger;
    m_mergedwidget = nullptr;
    m_unmergedwidget = nullptr;
    ui->tabWidget->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_merger;
}


void MainWindow::on_openfile_btn_clicked()
{
    m_file = QFileDialog::getOpenFileName(this, "Select a file", QDir::currentPath());
    if (m_file.isEmpty()) return;
    if (!m_merger->readFile(m_file.toUtf8())) {
        QMessageBox::critical(this, "Error", "Can't open file");
    }
    ui->filename_label->setText(m_file);
    ui->tabWidget->show();
    ui->tabWidget->removeTab(0);
    if (m_mergedwidget) {
        delete m_mergedwidget;
        m_mergedwidget = nullptr;
    }
    if (m_unmergedwidget) {
        delete m_unmergedwidget;
        m_unmergedwidget = nullptr;
    }
    if (m_merger->isMerged()) on_mergedFile();
    else on_unmergedFile();
}

void MainWindow::on_mergedFile()
{
    m_mergedwidget = new MergedWidget(this);
    m_mergedwidget->setMerger(m_merger);
    ui->tabWidget->addTab(m_mergedwidget, "Merged");
}

void MainWindow::on_unmergedFile()
{
    m_unmergedwidget = new UnmergedWidget(this);
    m_unmergedwidget->setMerger(m_merger);
    ui->tabWidget->addTab(m_unmergedwidget, "Not merged");
}
