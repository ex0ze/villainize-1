#include "filemerger.h"

FileMerger::FileMerger() : m_filetype(UNDEFINED) , m_readFlag(false)
{
    memset(&m_info, 0, sizeof m_info);
}

bool FileMerger::readFile(const char *filename)
{
    if (!fileExists(filename)) return false;
    std::ifstream in(filename, std::ios::binary);
    if (!in.is_open()) return false;
    m_mainpart.clear();
    m_mergedpart.clear();
    m_filetype = UNDEFINED;
    m_mainfilename = filename;
    std::size_t fsize = fileSize(filename);
    if (grabHeader(in, fsize)) {
        m_filetype = MERGED;
        m_mergedfilename = m_info.file_name;
        splitFiles(in, fsize);
    }
    else {
        m_filetype = UNMERGED;
        m_mainpart.resize(fsize);
        in.read(reinterpret_cast<char*>(m_mainpart.data()), fsize);
    }
    m_mainfileformat = (m_mainfilename.find_last_of(".") == std::string::npos) ? std::string() : m_mainfilename.substr(m_mainfilename.find_last_of("."));
    m_readFlag = true;
    in.close();
    return true;
}

bool FileMerger::writeFile(const char *filename)
{
    if (m_mainpart.empty()) return false;
    std::ofstream out(filename, std::ios::binary);
    if (m_mergedpart.empty()) {
        out.write(reinterpret_cast<char*>(m_mainpart.data()), m_mainpart.size());
    }
    else {
        std::vector<unsigned char> file_data;
        std::copy(m_mainpart.begin(), m_mainpart.end(), std::back_inserter(file_data));
        std::copy(m_mergedpart.begin(), m_mergedpart.end(), std::back_inserter(file_data));
        std::size_t oldSize = file_data.size();
        file_data.resize(oldSize + sizeof(merged_file_info_t));
        std::vector<unsigned char> m_info_data(sizeof(merged_file_info_t));
        memcpy(m_info_data.data(), &m_info, sizeof(merged_file_info_t));
        for (auto &x:m_info_data) x ^= 0x33;
        memcpy(&m_info, m_info_data.data(), sizeof(merged_file_info_t));
        memcpy(&file_data[oldSize], &m_info, sizeof(merged_file_info_t));
        out.write(reinterpret_cast<char*>(file_data.data()), file_data.size());
    }
    out.close();
    return true;
}

bool FileMerger::writeMergedpart(const char *filename)
{
    if (m_mergedpart.empty()) return false;
    std::ofstream out(filename, std::ios::binary);
    out.write(reinterpret_cast<char*>(m_mergedpart.data()), m_mergedpart.size());
    out.close();
    return true;
}

bool FileMerger::cutMergedpart(const char *filename)
{
    writeMergedpart(filename);
    m_mergedpart.clear();
    writeFile(m_mainfilename.c_str());
}

bool FileMerger::writeMainpart(const char *filename)
{
    if (m_mainpart.empty()) return false;
    std::ofstream out(filename, std::ios::binary);
    out.write(reinterpret_cast<char*>(m_mainpart.data()), m_mainpart.size());
    out.close();
    return true;
}

bool FileMerger::isOpened() const
{
    return m_readFlag;
}

bool FileMerger::isPasswordProtected() const
{
    return m_info.encryption_flag;
}

bool FileMerger::isMerged() const
{
    return (!m_mergedpart.empty());
}

bool FileMerger::mergeFile(const char *filename)
{
    if (!fileExists(filename)) return false;
    m_mergedfilename = filename;
    m_mergedfilename = std::string(std::find_if(m_mergedfilename.rbegin(), m_mergedfilename.rend(),MatchPathSeparator()).base(),
                                   m_mergedfilename.end());
    std::ifstream in(filename, std::ios::binary);
    std::size_t fsize = fileSize(filename);
    m_mergedpart.resize(fsize);
    in.read(reinterpret_cast<char*>(m_mergedpart.data()), fsize);
    in.close();
    create_mfit(&m_info, 1, m_mergedfilename.c_str(), fsize, fsize);
    return true;
}

bool FileMerger::setPassword(const char *pass)
{
    std::string _pass(pass);
    if (_pass.empty()) return false;
    QCryptographicHash hasher_sha512(QCryptographicHash::Algorithm::Sha512);
    QCryptographicHash hasher_sha256(QCryptographicHash::Algorithm::Sha256);
    hasher_sha512.addData(_pass.data(),_pass.length());
    auto sha512_phash = hasher_sha512.result();
    hasher_sha256.addData(_pass.data(), _pass.length());
    auto sha256_phash = hasher_sha256.result();
    std::size_t oldLen = m_mergedpart.size();
    applyPadding();
    struct AES_ctx ctx;
    unsigned char initialization_vector[16];
    fillIv(initialization_vector);
    AES_init_ctx_iv(&ctx, reinterpret_cast<unsigned char*>(sha256_phash.data()),initialization_vector);
    AES_CBC_encrypt_buffer(&ctx, m_mergedpart.data(), m_mergedpart.size());
    create_mfit(&m_info, 1, m_mergedfilename.data(), m_mergedpart.size(), oldLen, true, sha512_phash.constData(), initialization_vector);
    return true;
}

bool FileMerger::enterPassword(const char *pass)
{
    if (m_mergedpart.empty()) return false;
    std::string _pass(pass);
    QCryptographicHash hasher_sha512(QCryptographicHash::Algorithm::Sha512);
    QCryptographicHash hasher_sha256(QCryptographicHash::Algorithm::Sha256);
    hasher_sha512.addData(_pass.data(),_pass.length());
    QByteArray _pass_cmp(m_info.encryption_hash_sha512,64);
    auto sha512_phash = hasher_sha512.result();
    if (_pass_cmp != sha512_phash) return false;
    hasher_sha256.addData(_pass.data(),_pass.length());
    auto sha256_phash = hasher_sha256.result();
    struct AES_ctx ctx;
    AES_init_ctx_iv(&ctx, reinterpret_cast<unsigned char*>(sha256_phash.data()),m_info.enctyption_iv);
    AES_CBC_decrypt_buffer(&ctx, m_mergedpart.data(), m_mergedpart.size());
    m_mergedpart.resize(m_info.file_size_nopadding);
    return true;
}

void FileMerger::clearMainpart()
{
    m_mainpart.clear();
}

void FileMerger::clearMergedpart()
{
    m_mergedpart.clear();
}

const char *FileMerger::mergedFilename() const
{
    return m_mergedfilename.c_str();
}

const char *FileMerger::mainFilename() const
{
    return m_mergedfilename.c_str();
}

const char *FileMerger::mainFileformat() const
{
    return m_mainfileformat.c_str();
}

std::size_t FileMerger::fileSize(const char *filename)
{
    std::ifstream in(filename, std::ios::binary);
    in.seekg(0, std::ios::end);
    std::size_t _size = in.tellg();
    in.close();
    return _size;
}

bool FileMerger::fileExists(const char *filename)
{
    return (access(filename, 0) == 0);
}


bool FileMerger::grabHeader(std::ifstream &in, std::size_t sz)
{
    if (sz < sizeof(merged_file_info_t)) return false;
    std::vector<unsigned char> header_data;
    header_data.resize(sizeof(merged_file_info_t));
    std::size_t last_pos_g = in.tellg();
    in.seekg(sz - sizeof(merged_file_info_t));
    in.read(reinterpret_cast<char*>(header_data.data()),sizeof(merged_file_info_t));
    for (auto &x : header_data) x ^= 0x33;
    memcpy(&m_info, header_data.data(), header_data.size());
    in.seekg(last_pos_g, std::ios::beg);
    return is_valid_mfit_ident(&m_info, nullptr);
}

bool FileMerger::splitFiles(std::ifstream &in, std::size_t sz)
{
    m_mainpart.resize(sz - sizeof(merged_file_info_t) - m_info.file_size);
    in.seekg(0,std::ios::beg);
    in.read(reinterpret_cast<char*>(m_mainpart.data()), m_mainpart.size());
    in.seekg(m_mainpart.size(), std::ios::beg);
    m_mergedpart.resize(m_info.file_size);
    in.read(reinterpret_cast<char*>(m_mergedpart.data()),m_mergedpart.size());
    return true;
}

void FileMerger::fillIv(unsigned char *data)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 255);
    for (std::size_t i = 0;i < 16;++i) {
        data[i] = dis(gen);
    }
}

void FileMerger::applyPadding()
{
    std::size_t partsCount = m_mergedpart.size() / 16ull;
    if (m_mergedpart.size() % 16ull > 0) {
        partsCount++;
    }
    m_mergedpart.resize(partsCount * 16ull);
}
